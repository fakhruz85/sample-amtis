<?php
namespace SampleAmtis\Htmlv2;

class Table {
  public $title = "";
  public $numRows = 0;

  public function message() {
    echo "<p>htmlv2 - Table '{$this->title}' has {$this->numRows} rows.</p>";
  }

  public function tStr(){
    return "<table border='1' width='50vw' height='50vh'>";
  }

  public function tEnd(){
    return "</table>";
  }

  public function generate($data){
    $row = count($data);
    $col = count($data[0]);
    $strTable = '';

    $i = 0;
    for(;$i<$row;$i++){
        $j = 0;
        $r = new Row();

        $strTable .= $r->rStr();
        for(;$j<$col;$j++){
          $c = new Column();
          $strTable .= $c->d(['out'=>$data[$i][$j],'style'=>'background-color:#cccc']);
        }

        $strTable .= $r->rEnd();
    }
    echo $this->tStr().$strTable.$this->tEnd();
  }

  public function generateSudoku($data){
    $row = count($data);
    $col = count($data[0]);
    $strTable = '';
    $new_group = false;

    $bgColor = ["red", "green", "blue"];
    $curBgColor = current($bgColor);

    $i = 0;
    for(;$i<$row;$i++){
        $j = 0;

        if ($i <= 2) {
            $row_num = 1;
        }
        if ($i > 2 && $i <= 5) {
            $row_num = 2;

            if($temp_row_num != $row_num)
              $new_group = true;
        }
        if ($i > 5 && $i <= 8) {
            $row_num = 3;

            if($temp_row_num != $row_num)
              $new_group = true;
        }

        if($new_group){
          $curBgColor = next($bgColor);
          $new_group = false;
        }

        $temp_row_num = $row_num;

        $r = new Row();

        $strTable .= $r->rStr();
        for(;$j<$col;$j++){
          $c = new Column();
          $strTable .= $c->d(['out'=>$data[$i][$j],'style'=>'background-color:'.$curBgColor]);
        }

        $strTable .= $r->rEnd();
    }
    echo $this->tStr().$strTable.$this->tEnd();
  }
}

class Row {
  public $numCells = 0;
  public function message() {
    echo "<p>htmlv2 - The row has {$this->numCells} cells.</p>";
  }

  public function rStr(){
    return "<tr>";
  }

  public function rEnd(){
    return "</tr>";
  }
}

class Column{
  protected $data = "";
  public function getData(){
    echo "<p>htmlv2 - The column has {$this->data}.</p>";
  }

  public function cStr(){
    return "<td>";
  }

  public function cEnd(){
    return "</td>";
  }

  public function d($data){
    $style = $data['style']??'';

    return "<td style='{$style}'>".$data['out']."</td>";
  }
}
?>