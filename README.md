# sample-amtis
Generate html element template

## Installation
This project using composer.
```
$ composer require fakhruz85/sample-amtis
```

## Usage
Generate html element template.
```php
<?php

use SampleAmtis\Htmlv2\Table;

$data = [['1','2','3'],['1','2','3']];
$table = new Table();
$table->generate($data);